<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-180172331-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-180172331-1');
    </script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>Picadas Macanudas :: sabores que compartimos</title>
    <meta name="description" content="Picadas en Mendoza. Acompañá tus buenos momentos con nuestras picadas.">
    <meta name="keywords" content="">
    <!-- Favicons -->
    <link href="assets/img/favicon.png" rel="icon">
    <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,600,600i,700,700i|Satisfy|Comic+Neue:300,300i,400,400i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Piazzolla:ital,wght@0,300;0,800;0,900;1,500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
    <style>
        @font-face {
            font-family: "Nickainley";
            src: url("assets/font/Nickainley-Normal.otf");
        }
    </style>

    <!-- Vendor CSS Files -->
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
    <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
    <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
    <!-- Template Main CSS File -->
    <link href="assets/css/style.css" rel="stylesheet">
</head>

<body>
    <!-- ======= Top Bar ======= -->
    <section id="topbar" class="d-none d-lg-flex align-items-center fixed-top topbar-transparent">
        <div class="container text-right">
            <i class="icofont-phone"></i> +54 9 2613 40-5502
            <i class="icofont-clock-time icofont-rotate-180"></i> Lun-Sáb: 10:00 AM - 20:00 PM
        </div>
    </section>
    <!-- ======= Modal Publicidad ======= -->
    <!--<div id="myModal" class="modal fade">
        <div class="modal-dialog modal-dialog-centered modal-lg ">
            <div class="modal-content border-warning mt-5 mb-5">
                <div class="modal-body">
                    <div class="section-title">
                        <div class="row">
                            <div class="col-sm-11">
                                <h2>Novedades de<span> fin de año</span> </h2>
                            </div>
                            <div class="col-sm-1 align-self-center">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="owl-carousel events-carousel mb-3 ">
                    <div class="row event-item">
                        <div class="col-lg-6">
                            <img src="assets/img/espumante-pacto.jpg" class="img-fluid" alt="">
                        </div>
                        <div class="col-lg-6 pt-4 pt-lg-0 content">
                            <h2>Vino Espumante:</h2>
                            <p class="m-0">Pinot Noir brut nature</p>
                            <span class="badge badge-warning">Partida Limitada</span>
                            <br>
                            <br>
                            <h3 style="text-align: center;"><strong>Agragalo <br> a tus Pedidos de Picadas!</strong>
                            </h3>
                            <p class="text-warning" style="text-align: center; font-size: larger;">$ 500.00</p>
                            <br>
                        </div>
                    </div>
                    <div class="row event-item">
                        <div class="col-lg-6">
                            <img src="assets/img/box-macanuda.jpeg" class="img-fluid" alt="">
                        </div>
                        <div class="col-lg-6 pt-4 pt-lg-0 content">
                            <h2>Box de finde año:</h2>
                            <p>
                                - Espumante 🍾<br>
                                - Chorizo seco 250 gr 🍖<br>
                                - Horma de queso saborizado 🧀800 gr <br>
                                - Grisines caseros 🥖<br>
                                - Mix de chocolates 🍫-<br>
                            </p>
                            <h3 style="text-align: center;">$ <strong>3400,00</strong></h3>
                        </div>
                    </div>
                    <div class="row event-item">
                        <div class="col-lg-6">
                            <img src="assets/img/bolsa-macanuda.jpeg" class="img-fluid" alt="">
                        </div>
                        <div class="col-lg-6 pt-4 pt-lg-0 content">
                            <h2>Bolsa de finde año:</h2>
                            <p>
                                - Espumante 🍾<br>
                                - Chorizo seco 250 gr 🍖<br>
                                - Horma de queso saborizado 🧀800 gr <br>
                                - Grisines caseros 🥖<br>
                                - Mix de chocolates 🍫<br>
                            </p>
                            <h3 style="text-align: center;">$ <strong>2600,00</strong></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>-->
    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top d-flex align-items-center header-transparent">
        <a href="index.php"><img style="width: 25%; margin-left:25%;" src="assets/img/MCND_Logo_1080_WHITE.png" alt="picadas mendoza"></a>
        <div class="container d-flex align-items-center">

            <div class="logo mr-auto">
                <h1 class="text-light"><a href="index.php"></a></h1>
                <!-- Uncomment below if you prefer to use an image logo -->
                <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
            </div>
            <nav class="nav-menu d-none d-lg-block">
                <ul>
                    <li class="active"><a href="index.php">Inicio</a></li>
                    <li><a href="#about">Nosotros</a></li>
                    <li><a href="#menu">Picadas</a></li>
                    <li><a href="#events">Agregados</a></li>
                    <li><a href="#gallery">Galeria</a></li>
                    <li><a href="#contact">Contacto</a></li>
                </ul>
            </nav><!-- .nav-menu -->
        </div>
    </header><!-- End Header -->
    <!-- ======= Hero Section ======= -->
    <?php
    $conn = mysqli_connect(
        '31.170.161.22', //31.170.161.22
        'u101685278_labmac', // u101685278_pachimanok
        'Rail2021', //Pachiman9102
        'u101685278_labmac' //u101685278_macanudas
    );
?>
    <?php include('sections/headSection.php'); ?>
    <main id="main">
    <?php include('sections/quienesSomos.php'); ?>
    <?php include('sections/nuestrasPicadas.php'); ?> 
    <?php include('sections/acompañamientos.php'); ?>
    <?php include('sections/cotizador.php'); ?>

        
        <section id="specials" class="specials">
            <div class="container">
                <div class="section-title pb-0">
                    <h2>La opinion de nuestros <span>Clientes Felices</span></h2>

                    <hr>
                </div>
                <div class="owl-carousel events-carousel">
                    <?php
                    $query_satisfaction = "SELECT satisfaction_servicio, product, cnee, comentario_pagina  FROM `general` WHERE pagina = 'si' ORDER BY created_at DESC LIMIT 5";
                    $result_satifaction = mysqli_query($conn, $query_satisfaction);

                    while ($rows = mysqli_fetch_assoc($result_satifaction)) {

                        $servicio = $rows['satisfaction_servicio'];
                        $profuct = $rows['product'];
                        $cnee = $rows['cnee'];
                        $comentario = $rows['comentario_pagina'];

                        if ($servicio == 1) {
                            $servicio = '<i class="fa fa-star text-warning p-1" ></i><i class="fa fa-star text-secondary p-1" ></i><i class="fa fa-star text-secondary p-1" ></i><i class="fa fa-star text-secondary p-1" ></i><i class="fa fa-star text-secondary " ></i>';
                        } elseif ($servicio == 2) {
                            $servicio = '<i class="fa fa-star text-warning p-1" ></i><i class="fa fa-star text-warning p-1" ></i><i class="fa fa-star text-secondary p-1" ></i><i class="fa fa-star text-secondary p-1" ></i><i class="fa fa-star text-secondary " ></i>';
                        } elseif ($servicio == 3) {
                            $servicio = '<i class="fa fa-star text-warning p-1" ></i><i class="fa fa-star text-warning p-1" ></i><i class="fa fa-star text-warning p-1" ></i><i class="fa fa-star text-secondary p-1" ></i><i class="fa fa-star text-secondary " ></i>';
                        } elseif ($servicio == 4) {
                            $servicio = '<i class="fa fa-star text-warning p-1" ></i><i class="fa fa-star text-warning p-1" ></i><i class="fa fa-star text-warning p-1" ></i><i class="fa fa-star text-warning p-1" ></i><i class="fa fa-star text-secondary " ></i>';
                        } else {
                            $servicio = '<i class="fa fa-star text-warning p-1" ></i><i class="fa fa-star text-warning p-1" ></i><i class="fa fa-star text-warning p-1" ></i></i><i class="fa fa-star text-warning p-1" ></i><i class="fa fa-star text-warning " ></i>';
                        }
                    ?>
                        <div class="row event-item">
                            <div class="col-lg-8 mx-auto">
                                <div class="row">
                                    <div class="col-sm-3 mx-auto" style="text-align: center;">
                                        <?php echo $servicio; ?>
                                    </div>
                                </div>
                                <br>
                                <p style="text-align: center;"><?php echo $comentario; ?></p>
                                <p class="font-italic mb-0" style="text-align: center;">
                                    <strong><?php echo $cnee; ?></strong></span>
                                </p>
                                <p class="font-italic" style="text-align: center;"><?php echo $profuct; ?></span></p>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <hr>
            </div>

        </section>
        <?php include('sections/medios.php'); ?>
        <?php include('sections/galeria.php'); ?>


    </main><!-- End #main -->

    <!-- ======= Footer ======= -->
    <footer id="footer">
        <div class="container">

            <section id="contact" class="contact">
                <div class="container">

                    <div class="section-title">
                        <h2><span>Contactanos</span></h2>
                        <p>Por cualquiera de estos medios o dejanos un mensaje.</p>
                    </div>
                </div>

                <div class="map">

                    <iframe style="border:0; width: 100%; height: 350px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13398.88694727627!2d-68.81595921488713!3d-32.905524643435726!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x967e095d1db5e213%3A0xbc752ee160420b8!2sAmado%20Nervo%2C%20Mendoza!5e0!3m2!1ses!2sar!4v1598278104723!5m2!1ses!2sar" frameborder="0" allowfullscreen></iframe>
                </div>

                <div class="container mt-5">

                    <div class="info-wrap">
                        <div class="row">
                            <div class="col-lg-3 col-md-6 info">
                                <i class="icofont-google-map"></i>
                                <h4>Localización:</h4>
                                <p>Amado Nervo y Dorrego <br>Mendoza, Argentina</p>
                            </div>
                            <div class="col-lg-3 col-md-6 info mt-4 mt-lg-0">
                                <i class="icofont-clock-time icofont-rotate-90"></i>
                                <h4>Horarios:</h4>
                                <p>Lunes-Sabado:<br>10:00 AM - 20:00 PM</p>
                            </div>
                            <div class="col-lg-3 col-md-6 info mt-4 mt-lg-0">
                                <i class="icofont-phone"></i>
                                <h4>Whats App:</h4>
                                <p>+54 9 2613 40-5502<br></p>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- End Contact Section -->

            </main><!-- End #main -->

            <!-- ======= Footer ======= -->
            <footer id="footer">
                <div class="container">
                    <a href="index.php"><img style="width: 25%;" src="assets/img/MCND_Logo_1080_WHITE.png" alt=""></a>
                    <div class="social-links">
                        <a href="https://api.whatsapp.com/send?phone=542613405502&text=Hola,%20te%20contacto%20desde%20la%20Página" class="twitter"><i class="bx bxl-whatsapp"></i></a>
                        <a href="https://www.facebook.com/picadasmacanudas" class="facebook"><i class="bx bxl-facebook"></i></a>
                        <a href="https://www.instagram.com/picadasmacanudas" class="instagram"><i class="bx bxl-instagram"></i></a>

                    </div>
                    <div class="copyright">
                        &copy; Copyright <strong><span>Picadas Macanudas</span></strong>. All Rights Reserved
                    </div>
                    <div class="credits">
                        <!-- All the links in the footer should remain intact. -->
                        <!-- You can delete the links only if you purchased the pro version. -->
                        <!-- Licensing information: https://bootstrapmade.com/license/ -->
                        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/delicious-free-restaurant-bootstrap-theme/ -->
                        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a> & <a href="">BUILDIT</a>
                    </div>
                </div>
            </footer><!-- End Footer -->
            <a href="https://api.whatsapp.com/send?phone=542613405502" target="_blank" style="color:#433f39; text-align:center;" class="whatsapp"><i class="bx bxl-whatsapp"></i>Contactanos!</a>
            <!--a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a-->
            <!-- Vendor JS Files -->
            <script src="assets/vendor/jquery/jquery.min.js"></script>
            <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
            <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
            <script src="assets/vendor/php-email-form/validate.js"></script>
            <script src="assets/vendor/jquery-sticky/jquery.sticky.js"></script>
            <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
            <script src="assets/vendor/venobox/venobox.min.js"></script>
            <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>            
            <script>
                $(function() {
                    $('[data-toggle="popover"]').popover()
                })
                $(function() {
                    $('[data-toggle="tooltip"]').tooltip()
                });
            </script>

            <!-- Template Main JS File -->
            <script src="assets/js/main.js"></script>

            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
            
            <!-- <script>
                $(document).ready(function() {
                    $("#myModal").modal('show');
                });
            </script> -->
            <?php include('prueba_modal.php'); ?>
</body>

</html>