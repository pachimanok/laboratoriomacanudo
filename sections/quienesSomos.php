<!-- ======= About Section ======= -->
<section id="about" class="about">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-5" style="padding: 0px;">
                <img src="<?php echo /* $_SERVER['DOCUMENT_ROOT'] */ 'http://localhost/laboratoriomacanudo/' ?>assets/img/Macanudas-85.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch">
                <div class="content">
                    <h3 style="color: #ffb03b;"><strong>¿Quiénes Somos?</strong></h3>
                    <p>sections/quienesSomos.php
                        Somos una familia Mercedina de origen, mendocina por adopción que ante el grito
                        de
                        nuestros amigos mendocinos de “cuando vayas a Mercedes tráeme salame!” decidimos
                        compartir estos tradicionales sabores del campo pampeano.</p>
                    </p>
                    <div class="text-center"><button type="submit" class="btn btn-outline-secondary" data-toggle="modal" data-target="#macanudas">¿Por qué macanudas?</button>
                    </div>
                    <br>
                    <h3 style="color: #ffb03b;"><strong>Mercedes</strong></h3>

                    <p class="font-italic">
                        Mercedes es una ciudad situada a 100 km al oeste de la Capital Federal, y es
                        sede de la
                        Fiesta Nacional del Salame Quintero.
                    </p>
                    <div class="text-center">
                        <button type="submit" class="btn btn-outline-secondary" data-toggle="modal" data-target="#historia">un poco de Historia</button>
                    </div>
                    <div class="modal fade" id="historia" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-body" style="padding: 0%;">
                                    <br>
                                    <h3 style="text-align: center !important; color: #ffb03b;">Historia:
                                    </h3>
                                    <br>
                                    <img style="max-width: -webkit-fill-available;" src="<?php echo /* $_SERVER['DOCUMENT_ROOT'] */ 'http://localhost/laboratoriomacanudo/' ?>assets/img/Mercedes.png" alt="mrecedes">
                                    <br>
                                    <br>
                                    <p style="margin: 0% 15% 5%; text-align: center;">Durante el siglo
                                        XIX y
                                        principio del XX, arribaron a la zona inmigrantes de la
                                        Península
                                        itálica e ibérica, una vez instalados en sus llamadas “quintas o
                                        chacras” introdujeron en estas tierras la artesanía culinaria
                                        del
                                        chacinado que ancestralmente fueron transmitidas de generación
                                        en
                                        generación y que por esas razones convergentes, de buena carne
                                        porcina,
                                        mezclada a la excelente carne bovina, sumando a las aguas, el
                                        clima y
                                        las especies, comenzó a desarrollarse un llamado “salame
                                        quintero” que
                                        tenía particularidades de color y sabor que se afincaron en
                                        nuestra
                                        zona, para trascender en el gusto de la gente hasta hacerse
                                        conocido el
                                        dicho que los mejores salames son los de la zona de Mercedes.
                                    </p>

                                    <p style="margin: 0% 15% 5%; text-align: center;"> Es así que el
                                        salame
                                        quintero se convirtió en el pretexto para que familias o amigos
                                        se
                                        reúnan en interminables tertulias, donde los chacinados y la
                                        charla
                                        sirven para pasar un grato momento.</p>


                                    <div class="text-center">
                                        <button type="button" class="btn btn-outline-secondary rounded-circle" data-dismiss="modal">X</button>
                                    </div>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="modal fade" id="macanudas" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-body" style="padding: 0%;">
                                    <br>
                                    <h3 style="text-align: center !important; color: #ffb03b;">¿Por qué
                                        Macanudas?</h3>
                                    <br>
                                    <p style="margin: 0% 15% 1%; text-align: center;">Es un
                                        emprendimiento
                                        matrimonial y, si hay algo que es parte de nuestra tradición y
                                        nuestra
                                        historia familiar, es el gusto por las historietas del dibujante
                                        argentino, Liniers.</p>
                                    <p style="margin: 0% 15% 1%; text-align: center;">Siendo novios, y
                                        después
                                        de casados también, coleccionamos sus libros y todo lo que
                                        encontrábamos
                                        con sus personajes. Parte de nuestro cotillón fueron los
                                        personajes
                                        principales de sus historias.</p>
                                    <p style="margin: 0% 15% 1%; text-align: center;">Cuando surgió este
                                        emprendimiento nos pareció un adjetivo que cerraba por todos
                                        lados:
                                        Habla de nosotros, de nuestros gustos, pero sobre todo de lo que
                                        queremos ser: ¡gente macanuda que hace picadas macanudas!</p>

                                    <br>
                                    <img style="max-width: -webkit-fill-available;" src="<?php echo /* $_SERVER['DOCUMENT_ROOT'] */ 'http://localhost/laboratoriomacanudo/' ?>assets/img/casorio_macanudo2.png" alt="">
                                    <br>
                                    <br>
                                    <hr style="width:70%;">
                                    <p style="margin: 0% 15% 1%; text-align: center;" class="font-italic">
                                        <strong> Macanudo: </strong> es la buena persona, alguien
                                        amable,
                                        compañero y que siempre está dispuesto a darte una mano. Es esa
                                        persona
                                        que a donde llega alegra el ambiente. Lo que caracteriza al buen
                                        argentino
                                    </p>
                                    <br>

                                    <div class="text-center">
                                        <button type="button" class="btn btn-outline-secondary rounded-circle" data-dismiss="modal">X</button>
                                    </div>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h3><strong>Nuestro Objetivo</strong></h3>
                    <p>
                        Nuestro objetivo es destacarnos por la <strong>calidad de los
                            productos</strong>, y es
                        por ello que para asegurarlo hacemos una selección personal contactándonos de
                        forma
                        <strong>directa con los productores</strong>.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section><!-- End About Section -->