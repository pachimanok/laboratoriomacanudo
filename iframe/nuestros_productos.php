<?php

include('layouts/header.php');
$conn = mysqli_connect(
    '31.170.161.22',//31.170.161.22
    'u101685278_labmac',// u101685278_pachimanok
    'Rail2021',//Pachiman9102
    'u101685278_labmac' //u101685278_macanudas
  );

$q_productos = "SELECT * FROM `nuestros_productos`";
$r_productos = mysqli_query($conn, $q_productos);


  ?>
<section id="why-us" class="why-us">
    <div class="container">
        <div class="section-title">
            <h2>Nuestros<span> Productos</span></h2>
            <p>Las mejores combinaciones para tus picadas. Acá te describimos cada una de ellas.</p>
        </div>
        <div class="row">
            <?php                        
                        while($row = mysqli_fetch_assoc($r_productos)) { 
                                $id = $row['id'];
                                $title = $row['titulo'];
                                $subtitulo = $row['subtitulo'];
                                $imagen = $row['imagen'];
                                $quesos = $row['quesos'];
                                $fiambres = $row['fiambres']; 
                                $ademas = $row['ademas']; 
                       ?>
            <div class="col-lg-4">
                <div class="box" data-toggle="modal" data-target="#modal<?php echo $id; ?>">
                    <h1><?php echo $title ?></a></h1>
                    <p> <strong><?php echo $subtitulo; ?></strong></p>
                    <br>
                    <img src="../assets/img/picadas/<?php echo $imagen; ?>" style="max-width: -webkit-fill-available;">
                </div>
            </div>
            <div class="modal fade" id="modal<?php echo $id; ?>" tabindex="-1" role="dialog"
                aria-labelledby="scrollmodalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <br>
                            <h3 style="text-align: center !important; color: #ffb03b; margin-bottom: 0%;">Picada
                                <?php echo $title ?>
                            </h3>
                            <p style="text-align: center;"><?php echo $subtitulo ?></p>
                            <br>
                            <div class="card" style="text-align: center; margin: 0% 30% 3%; border: none;">
                                <h4>Quesos:</h4>
                                <?php echo $quesos ?>
                                <br>
                                <br>
                                <h4>Fiambres:</h4>
                                <?php echo $fiambres ?>
                                <br>
                                <br>
                                <h4>Además:</h4>
                                <?php echo $ademas?>
                                <br>
                                <br>
                                <h4>Pan Casero:</h4>
                            </div>
                            <img style="max-width: -webkit-fill-available; margin: 0% 6% 2%;"
                                src="../assets/img/picadas/<?php echo $imagen ?>" alt="">
                            <br>
                            <br>
                            <div class="text-center">
                                <button type="button" class="btn btn-outline-secondary rounded-circle"
                                    data-dismiss="modal">X</button>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>

    </div>
</section><!-- End Whu Us Section -->

<?php

include('layouts/footer.php');

?>