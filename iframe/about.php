<?php

include('layouts/header.php');
$conn = mysqli_connect(
    '31.170.161.22',//31.170.161.22
    'u101685278_labmac',// u101685278_pachimanok
    'Rail2021',//Pachiman9102
    'u101685278_labmac' //u101685278_macanudas
  ) or die(mysqli_erro($mysqli));

$q_about = "SELECT * FROM `quienes_somos`";
$r_about = mysqli_query($conn, $q_about);


while($row = mysqli_fetch_assoc($r_about)) { 
    
    $id = $row['id'];
    if($id == 1){
        $titleQs = $row['titulo'];
        $descripcionQs = $row['descripcion'];
        $botonQs = $row['titulo_2'];
        $desc_dosQs = $row['descripcion_2'];
        $epigrafeQs = $row['epigrafe']; 
        $imagenQs = $row['imagen']; 


    }elseif($id== 2){
        $titleM = $row['titulo'];
        $descripcionM = $row['descripcion'];
        $botonM = $row['titulo_2'];
        $desc_dosM = $row['descripcion_2'];
        $epigrafeM = $row['epigrafe'];
        $imagenM = $row['imagen']; 
        
    }else{
        $titleO = $row['titulo'];
        $descripcionO = $row['descripcion'];
        $botonO = $row['titulo_2'];
        $desc_dosO = $row['descripcion_2'];
        $epigrafeO = $row['epigrafe'];
        $imagenO = $row['imagen']; 
    }
    
}
  ?>

<section id="about" class="about">
<?php echo $imagenO;?>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-5" style="padding: 0px;">
                        <img src="https://picadasmacanudas.com/images/<?php echo $imagenO;?>" class="img-fluid" alt="">
                    </div>

                    <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch">

                        <div class="content">
                            <h3 style="color: #ffb03b;"><strong><?php  echo $titleQs; ?></strong></h3>
                            <p>
                               <?php echo $descripcionQs;?>
                            </p>
                            <div class="text-center"><button type="submit" class="btn btn-outline-secondary"
                                    data-toggle="modal" data-target="#macanudas"><?php echo $botonQs;?></button>
                            </div>
                            <br>

                            <h3 style="color: #ffb03b;"><strong><?php echo $titleM;?></strong></h3>

                            <p class="font-italic">
                            <?php echo $descripcionM;?>
                            </p>
                            <div class="text-center"><button type="submit" class="btn btn-outline-secondary"
                                    data-toggle="modal" data-target="#historia"><?php echo $botonM;?></button>
                            </div>
                            <div class="modal fade" id="historia" tabindex="-1" role="dialog"
                                aria-labelledby="scrollmodalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body" style="padding: 0%;">
                                            <br>
                                            <h3 style="text-align: center !important; color: #ffb03b;"><?php echo $botonM;?>
                                            </h3>
                                            <br>
                                            <img style="max-width: -webkit-fill-available;"
                                                src="../assets/img/<?php echo $imagenM;?>" alt="mrecedes">
                                            <br>
                                            <br>
                                                <p style="margin: 0% 15% 5%; text-align: center;"><?php echo $desc_dosM ?>
                                                <p style="margin: 0% 15% 5%; text-align: center;"><?php echo $epigrafeM ?>
                                            </p>
                                            <div class="text-center">
                                                <button type="button" class="btn btn-outline-secondary rounded-circle"
                                                    data-dismiss="modal">X</button>
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="modal fade" id="macanudas" tabindex="-1" role="dialog"
                                aria-labelledby="scrollmodalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">

                                        <div class="modal-body" style="padding: 0%;">
                                            <br>
                                            <h3 style="text-align: center !important; color: #ffb03b;"><?php echo $botonQs ?></h3>
                                            <br>
                                            <p style="margin: 0% 15% 1%; text-align: center;"> <?php echo $desc_dosQs?></p>

                                            <br>
                                            <img style="max-width: -webkit-fill-available;"
                                                src="../assets/img/<?php echo $imagenQs;?>" alt="">
                                            <br>
                                            <br>
                                            <hr style="width:70%;">
                                            <p style="margin: 0% 15% 1%; text-align: center;" class="font-italic">
                                            <?php echo $epigrafeQs?>
                                            </p>
                                            <br>

                                            <div class="text-center">
                                                <button type="button" class="btn btn-outline-secondary rounded-circle"
                                                    data-dismiss="modal">X</button>
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h3><strong><?php echo $titleO ?></strong></h3>
                            <p>
                            <?php echo $descripcionO ?>.
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </section>
<?php

include('layouts/footer.php');

?>