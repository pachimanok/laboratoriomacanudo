<?php

include('layouts/header.php');
$conn = mysqli_connect(
    '31.170.161.22',//31.170.161.22
    'u101685278_labmac',// u101685278_pachimanok
    'Rail2021',//Pachiman9102
    'u101685278_labmac' //u101685278_macanudas
);
$q_titulo = "SELECT * FROM `medios`";
$r_titulo = mysqli_query($conn, $q_titulo); ?>

<!-- ======= Medios Section ======= -->
<section id="specials" class="specials">
    <div class="container">
        <div class="section-title">
            <h2>Picadas Macanudas en <span>los medios</span></h2>
            <p>Estamos en los medios y participamos de diferentes eventos.</p>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <ul class="nav nav-tabs flex-column">
                    <?php 
                        
                        while ($row = mysqli_fetch_assoc($r_titulo)) {
                        $titulo = $row['titulo'];
                        $id = $row['id'];
                        $count = $count + 1;

                    ?>
                    <li class="nav-item">
                        <a class="nav-link active<?php if($count == 1){echo 'show';}?>" data-toggle="tab" href="#tab-<?php echo $id; ?>"><?php echo $titulo; ?></a>
                    </li>
                    <?php } ?>
            </div>
            <div class="col-lg-9 mt-4 mt-lg-0">
                    <?php 
                       while ($rows = mysqli_fetch_assoc($r_titulo)) {
                       $titulo = $rows['titulo'];
                       $id = $rows['id'];
                       $descripcion = $rows['descripcion'];
                       $subtitulo = $rows['subtitulo'];
                       $img = $rows['img'];
                       $link = $rows['link'];
                       
                       
                   ?>
                   <div class="tab-content">   
                    <div class="tab-pane active  <?php if($count == 1){echo ' show';}?>" id="tab-<?php echo $id ?>">
                        <div class="row">
                            <div class="col-lg-8 details order-2 order-lg-1">
                                <h3><?php echo $titulo; ?></h3>
                                <p class="font-italic"><?php echo $subtitulo; ?></p>
                                <p><?php echo $descripcion; ?></p>
                            </div>
                            <div class="col-lg-4 text-center order-1 order-lg-2">
                                <a href="<?php echo $link; ?>" target="_blank">
                                    <img src="../assets/img/<?php $img ;?>" alt="" class="img-fluid">
                                </a>
                            </div>
                        </div>
                    </div>
                   
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
<?php

include('layouts/footer.php');

?>