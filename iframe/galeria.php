<?php
include('layouts/header.php');

$conn = mysqli_connect(
    '31.170.161.22', //31.170.161.22
    'u101685278_labmac', // u101685278_pachimanok
    'Rail2021', //Pachiman9102
    'u101685278_labmac' //u101685278_macanudas
);

$q_galeria = "SELECT * FROM `galeria`";
$r_galeria = mysqli_query($conn, $q_galeria);
?>
<section id="gallery" class="gallery">
    <div class="container-fluid">
        <div class="section-title">

            <h2>Algunas imágenes de nuestros <span>Productos</span></h2>
            <p>Sabores que compartimos </p>
        </div>
        <div class="row no-gutters">
            <?php
            while ($row = mysqli_fetch_assoc($r_galeria)) {
                $img = $row['img'];
            ?>
                <div class="col-lg-3 col-md-4">
                    <div class="gallery-item">
                        <a href="#" class="venobox" data-gall="gallery-item">
                            <img src="../assets/img/slide/Galeria/<?php echo $img; ?>" alt="" class="img-fluid">
                        </a>
                    </div>
                </div>
            <?php  }  ?>
        </div>
    </div>
</section><!-- End Gallery Section -->
<!-- End Whu Us Section -->

<?php

include('layouts/footer.php');

?>